

<body class="is-preload">

<!-- Header -->
<section id="header" style="overflow:hidden">
<header>
<span class="image avatar"><img src="<?php echo get_field('logo_img'); ?>" alt="" /></span>
<h1 id="logo"><a href="#" style="color:black"><?php echo get_field('brand_name'); ?></a></h1>
<p style="color:grey"><?php echo get_field('slogan'); ?><br />
<?php echo get_field('short_des'); ?></p>
</header>
<nav id="nav">
<ul>
<li><a href="#one" class="active">About Fin</a></li>
<li><a href="#two">Information</a></li>
<li><a href="#three">Products</a></li>
<li><a href="#four">Order</a></li>
<li><a href="#five">Contact</a></li>
</ul>
</nav>
<footer>
<ul class="icons">
<?php if(get_field('twitter_shop')) { ?>
    <li><a href="<?php echo get_field('twitter_shop'); ?>" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
    <?php } ?>
    
    <?php if(get_field('facebook_shop')) { ?>
        <li><a href="<?php echo get_field('facebook_shop'); ?>" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
        <?php } ?>
        
        <?php if(get_field('instagram_shop')) { ?>
            <li><a href="<?php echo get_field('instagram_shop'); ?>" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
            <?php } ?>
            
            <?php if(get_field('email_shop')) { ?>
                <li><a href="mailto:<?php echo get_field('email_shop'); ?>" class="icon fa-envelope"><span class="label">Email</span></a></li>
                <?php } ?>
                
                </ul>
                </footer>
                </section>
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <!-- Wrapper -->
                <div id="wrapper">
                
                <!-- Main -->
                <div id="main">
                
                <!-- One -->
                <section id="one">
                <div class="image main" data-position="center">
                <img src=" <?php echo get_field('cover_image'); ?>" alt="" />
                </div>
                <div class="container">
                <?php echo get_field('content_1'); ?>
                </div>
                </section>
                
                <!-- Two -->
                <section id="two">
                <div class="container">
                <?php echo get_field('content_2'); ?>
                </div>
                </section>
                
                <!-- Three -->
                <section id="three">
                <div class="container">
                
                <div class="features">
                <?php echo get_field('product_list_des'); ?>
                <?php
                
                // check if the repeater field has rows of data
                if( have_rows('product_repeater') ):
                    
                    // loop through the rows of data
                    while ( have_rows('product_repeater') ) : the_row();
                    
                    $product = wc_get_product(get_sub_field('product_list'));
                    
                    ?>
                    <article>
                    <a class="image"><img src="<?php echo get_the_post_thumbnail_url( $product->get_id(), 'full' ); ?>" alt="" /></a>
                    <div class="inner">
                    
                    <h4><?php echo $product->get_name(); ?></h4>
                    <p><?php echo $product->get_description(); ?></p>	
                    <h4 class="price">฿ <?php echo $product->get_price(); ?></h4>
                    </div>
                    </article>
                    <?php
                    
                endwhile;
                
                else :
                    
                    // no rows found
                    
                endif;
                
                ?>
                
                
                </div>
                </div>
                </section>
                
                <!-- Four -->
                <section id="four">
                <div class="container">
                <?php echo get_field('order_des'); ?>

                <div class="row gtr-uniform">
                <div class="col-6 col-12-xsmall"><input type="text" name="name" id="name" placeholder="Name - Lastname" /></div>
                <div class="col-6 col-12-xsmall"><input type="text" name="tel" id="tel" placeholder="Tel." /></div>
                <div class="col-12 col-12-xsmall"><input type="email" name="email" id="email" placeholder="E-mail" /></div>
                <div class="col-6 col-12-xsmall">
                <select id="product">
                <option selected value="0">Choose Your Product</option>
                <?php 
                $args = array( 'post_type' => 'product', 'posts_per_page' => 99 );
                $loop = new WP_Query( $args );
                while ( $loop->have_posts() ) : $loop->the_post();
                the_title();
                echo '<option value="'.get_the_id().'">';
                the_title();
                echo '</option>';
            endwhile;
            ?>
            </select>
            </div>
            <div class="col-6 col-12-xsmall"><input type="text" name="count" id="count" placeholder="Amount" /></div>
            <div class="col-12"><textarea name="address" id="address" placeholder="Your Address" rows="6"></textarea></div>
            <div class="col-12">
            <ul class="actions">
            <li><input type="submit" class="primary" id="submit-order-btn" value="Confirm"/></li>
            <li><input type="reset" value="Reset" /></li>
            </ul>
            </div>
            </div>
       
            </div>
            </section>
            
            <?php endwhile; endif; ?>  
            <!-- Five -->
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <section id="five">
            <div class="container">
            <h3 style="color: red;">Contact Us</h3>
            
            <div class="row">
            <div class="col-6 col-12-xsmall">
            <?php echo get_field('address_shop'); ?>
            </div>
            <div class="col-6 col-12-xsmall">
            <b>Tel :</b> <a href="tel:<?php echo get_field('tel_shop'); ?>"><?php echo get_field('tel_shop'); ?> </a><br/>
            <b>Line ID :</b> <a href='http://line.me/ti/p/<?php echo get_field('line_shop'); ?>'> <?php echo get_field('line_shop'); ?></a> <br/>   
            <b>Facebook :</b> <a href="<?php echo get_field('facebook_shop'); ?>"> <?php echo get_field('facebook_shop'); ?></a> <br/>
            <b>Email :</b> <a href="mailto:<?php echo get_field('email_shop'); ?>"> <?php echo get_field('email_shop'); ?></a> <br/>
            </div>
            </div>
            <br>
            <div class="row">
            <?php echo get_field('map_shop'); ?>
            
            </div>
            </div>
            </section>
            </div>
            
            <!-- Footer -->
            <section id="footer">
            <div class="container">
            <ul class="copyright">
            <li>&copy; Powered by <a href="<?php echo site_url(); ?>">FIN.PET</a> All rights reserved 2019.</li>
            </ul>
            </div>
            </section>
            
            </div>
            <?php endwhile; endif; ?>  
           
            <!-- Scripts -->
            <?php get_footer(); ?>
            
            </body>
            
            </html>