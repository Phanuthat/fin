<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo get_bloginfo( 'name' ); ?></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/main.css" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/style.css" />
    <link rel="icon" href="https://image.flaticon.com/icons/png/128/1623/1623681.png" type="image/gif" sizes="16x16">
    <?php wp_head(); ?>
</head>